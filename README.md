# MySQL Cheatsheet

## Notas a tener en cuenta

- Un PK, debe ser `NOT NULL`.

- **En un `JOIN`** cuando creamos un `alias (AS)` en una tabla, luego no podemos especificar la tabla con su nombre original.

- `WHERE` vs. `HAVING`:

  - `WHERE`: Se utiliza para filtrar valores de una única fila.
  - `HAVING`: Se utiliza para filtrar valores de un `GROUP BY` (es decir, grupo de registros) o filtrar en base al resultado de una función agregada.

- Creación de tablas:

  - Especificar `UNIQUE` en las columnas correspondientes. Por ejemplo, en una columna `email` de una tabla `Cuentas` de una BD `RedSocial`, el correo electrónico debe ser único para el usuario.

  - Especificar valor `DEFAULT` en las columnas en las que lo veamos necesario. Por ejemplo, en el `stock` de un artículo.

## Sentencias útiles

#### Describir una tabla. Mostrar las características de cada columna.

```sql
DESCRIBE/DESC table;
```

#### Entrar en una BD

```sql
USE baseDeDatos;
```

#### Mostrar las tablas de la BD actual

```sql
SHOW TABLES;
```

#### Mostrar las BBDD

```sql
SHOW DATABASES;
```

#### Mostrar la sentencia de creación de una tabla

```sql
SHOW CREATE TABLE Jugador;
```

#### Eliminar una tabla

```sql
DROP TABLE tabla;
```

#### Eliminar una BD

```sql
DROP DATABASE tabla;
```

#### Crear una BD

```sql
CREATE DATABASE baseDeDatos;
```

## Consulta de datos

### SELECT

#### Mostrar las columnas especificadas de la tabla `Cliente`

```sql
SELECT nombre, dni, email FROM Cliente;
```

#### Mostrar _nueva_ columna _temporal_ con el resultado de `precio * stock`

```sql
SELECT nombre, precio*stock from Articulo;
```

#### CONCAT()

##### Mostrar _nueva_ columna _temporal_ `nombre_completo` que contenga la concatenación de las columnas especificadas

```sql
SELECT CONCAT(nombre, ' ', apellido1, ' ', apellido2) AS nombre_completo FROM Cliente;
```

#### TRIM()

##### Mostrar los columnas especificadas sin espacios alrededor

```sql
SELECT TRIM(nombre), TRIM(apellido1) FROM Cliente;
```

#### Alias `AS`

##### Mostrar un nombre distinto al original de una columna

```sql
SELECT CONCAT(lastName, " ", firstname) AS `Nombre Completo` FROM Cliente;
```

#### JOIN

##### INNER JOIN

```sql
SELECT nombre, apellido1, apellido2 FROM Cliente
INNER JOIN Compra ON Cliente.id_cliente=Compra.id_cliente
WHERE id_articulo=2;
```

```sql
SELECT nombre, apellido1, apellido2 FROM Cliente cl
INNER JOIN Compra co ON cl.id_cliente=co.id_cliente
WHERE id_articulo=2;
```

```sql
SELECT cl.nombre, apellido1, apellido2 FROM Cliente cl
INNER JOIN Compra co ON cl.id_cliente=co.id_cliente
INNER JOIN Articulo ar ON ar.id_articulo=co.id_articulo
WHERE ar.nombre LIKE '%camiseta%';
```

```sql
SELECT nombre, apellido1, apellido2 FROM Cliente cl
INNER JOIN Compra co ON cl.id_cliente=co.id_cliente
WHERE id_articulo=2;
```

## Modificación de datos

### INSERT

#### Insertar varios valores en la misma sentencia

```sql
INSERT INTO alumno(nombre, apellido1) VALUES('Fernando', 'Perez'), ('Asier', 'Garcia');
```

#### Insertar valores sin especificar el `ID`. Lo hacemos poniendo `NULL` en `ID`, funciona porque es `AUTO_INCREMENT`

- Debemos especificar todos los valores, ya sea con el valor «real» o con `NULL`

```sql
INSERT INTO Productor VALUES (NULL, "Roberto", "Lopez", 'Gomez', '1900-03-04', NULL);
```

#### Insertar datos en una tabla. El `ID PrimaryKey` no se especifica, ya que es auto-incremental

```sql
INSERT INTO alumno(nombre, apellido1) VALUES('Fernando', 'Perez');
```

### UPDATE

#### Actualizar el valor de una fila existente

```sql
UPDATE Jugador SET id_posicion = 1 WHERE jugador_id = 2;
```

#### Actualizar `peso=110` en el/los Jugador/es que se apelliden: García o Pérez

```sql
UPDATE Jugador SET peso=110 WHERE apellido2 IN ('Garcia','Perez');
```

#### Actualizar un valor en la columna `stock` donde `nombre == Camiseta*` (regexpr)

- LIKE es case-insensitive.

```sql
UPDATE Articulo SET stock=2 WHERE nombre LIKE 'Camiseta%';
```

#### Actualizar `description='Descripción'` donde el columna `description` sea `NULL`

```sql
UPDATE Articulo SET description='Pendiente de escribir' WHERE description IS NULL;
```

#### Actualizar `stock=5` donde `id_articulo` sea mayor que `0` y menor que `5`.

- Importante utilizar `AND` porque sino, no lo hace en ese rango en concreto.
- **Si se utiliza `OR` se sobrescribe todo**.

```sql
UPDATE Articulo SET stock=4 WHERE id_articulo > 0 AND id_articulo < 5;
```

### REPLACE()

#### Reemplazar parte del texto de un `VARCHAR`

```sql
UPDATE Cliente
SET email = REPLACE(email, '@disroot.org	', '@tutanota.com')
```

### DELETE

#### Eliminar la fila donde `alumno_id=1`

```sql
DELETE FROM alumno WHERE alumno_id=1;
```

## Funciones agregadas

### GROUP BY

#### Mostrar el `id_articulo` y la suma de la `cantidad` en base al `id_articulo`

```sql
SELECT id_articulo, SUM(cantidad) FROM Compra GROUP BY id_articulo;
```

#### HAVING

> Se utiliza para filtrar valores de un `GROUP BY` o filtrar en base al resultado de una función agregada, por ejemplo: `HAVING SUM(precio * ventas) > 2500`.

##### Mostrar el `precio total` que sea mayor de 3.500 € agrupado por `tipo_articulo` y `fabricante`

```sql
SELECT tipo_articulo, fabricante, SUM(precio) AS precio_total FROM Productos
GROUP BY tipo_articulo, fabricante
HAVING SUM(precio) > 850000;
```

## Gestión de tablas y BBDD

### CREATE DATABASE

#### Crear una BD

```sql
CREATE DATABASE baseDeDatos;
```

### CREATE TABLE

#### Crear tabla con `ID` tipo `numérico`, `auto-incremental`, `no-nulo` y `PK`

```sql
CREATE TABLE alumno (
	alumno_id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	nombre VARCHAR(30),
	apellido1 VARCHAR(30)
);
```

#### Crear una tabla `<N:M>` con FKs

```sql
CREATE TABLE Compra (
	id_compra INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_cliente INT NOT NULL,
	id_articulo INT NOT NULL,
	FOREIGN KEY (id_cliente) REFERENCES Cliente(id_cliente),
	FOREIGN KEY (id_articulo) REFERENCES Articulo(id_articulo)
);
```

#### DEFAULT

> Especifica el valor predeterminado de una columna. El valor especificado se agregará a todos los nuevos registros **si** no se especifica ningún valor.

##### Especificar un valor por defecto a la columna `stock`

```sql
CREATE TABLE Tabla (
	[...],
	email VARCHAR(30) UNIQUE,
	stock INT DEFAULT 0
);
```

### ALTER TABLE

#### MODIFY

##### Modificar el tipo de dato de una columna

- **Atención** al cambiar el tipo de dato, puede haber problemas de conversión.

```sql
ALTER TABLE Compra MODIFY fecha DATE;
```

##### Modificar el tipo de dato y constraint a varias columnas en la misma sentencia

```sql
ALTER TABLE Compra MODIFY id_cliente INT NOT NULL, MODIFY id_articulo INT NOT NULL;
```

#### DROP

##### Eliminar columna

```sql
ALTER TABLE Equipo DROP COLUMN mascota;
```

#### ADD

##### AFTER

###### Añadir columna en una posición en concreto

```sql
ALTER TABLE Jugador ADD nombre varchar(30) AFTER id_jugador;
```

##### Añadir `constraint UNIQUE()` a una columna existente

```sql
ALTER TABLE Personal ADD UNIQUE(dni);
```

#### Cambiar nombre a una columna existente

```sql
ALTER TABLE Cliente CHANGE CIF_NIF cif_nif varchar(9) not null;
```

### ADD

#### Añadir dos columnas a una tabla

```sql
ALTER TABLE Tabla
ADD entregado BOOLEAN,
ADD pagado BOOLEAN;
```

#### Añadir solo una FK a una columna existente

```sql
ALTER TABLE Musico
ADD FOREIGN KEY (id_instrumento) REFERENCES Instrumento(id_instrumento)
```

#### Añadir una columna FK en una tabla existente

```sql
ALTER TABLE Jugador
ADD id_equipo INT NOT NULL,
ADD FOREIGN KEY (id_equipo) REFERENCES Equipo(id_equipo);
```

### DROP INDEX

#### Eliminar un indice como: PK, FK, UNIQUE, etc.

##### Mostramos los indices de una tabla

```sql
SHOW INDEX FROM Personal;
```

##### Eliminamos el indice de la columna `dni` especificada

```sql
ALTER TABLE Personal DROP INDEX dni;
```

### DROP

#### Eliminar una tabla

```sql
DROP TABLE tabla;
```

#### Eliminar una BD

```sql
DROP DATABASE tabla;
```

## Funciones

### AVG()

#### Media precio de todos los artículos

```sql
SELECT AVG(precio) FROM Articulo;
```

### COUNT()

#### Contar cuantos cliente tienen un email de Disroot

```sql
SELECT COUNT(*) FROM Cliente WHERE email LIKE '%@disroot.org';
```

### SUM()

#### Suma los precios de todos los artículos

```sql
SELECT SUM(precio) FROM Articulo;
```

## Gestión de cuentas

### Crear usuario y darle permisos

#### Creamos un usuario con su contraseña

```sql
CREATE USER markel IDENTIFIED BY 'Markel123';
```

#### Damos permisos al usuario

##### Dar permisos al usuario en una sola tabla o BD en concreto (desde el usuario ROOT)

```sql
GRANT SELECT, UPDATE, DELETE ON sportSite.Articulo TO markel;
```

##### Dar todos los permisos al usuario

```sql
GRANT ALL ON *.* TO 'markel'@'%' WITH GRANT OPTION;
```

- `%`: indica la dirección IP. En este caso permite a todas las direcciones.
- `WITH GRANT OPTION`: indica que el usuario también puede dar permisos tipo GRANT.

## Exportación e importación de la BD

### Exportar BD

#### Exportamos la BD `sportSite` con el usuario `root`

```bash
mysqldump -u root -p sportSite > /home/markel/sportSite.bak.sql
```

### Importar BD

#### Importamos la BD `sportSite` con el usuario `root`

```sql
CREATE DATABASE sportSite;
```

```bash
mysql -u root -p sportSite < /home/markel/sportSite.bak.sql
```
